#pragma once

#include <libtextworker/libtextworker.h>
#include <libtextworker/get_config.h>
#include <libtextworker/interface/wxGUI.h>
#include <wx/arrstr.h>

using namespace libtextworker::get_config;
using namespace libtextworker::UI::wx;

GetConfig global_settings;
wxColorManager clrCall;
GetConfig editorCfg;

// libtextworker::TOPLV_DIR =

std::string appDir();
static wxArrayString recentsList;

// FIXME: As objects in JSONCpp have a .get which asks for
// a value of section/option is not found, GetConfig and TextWorker
// needs to have some changes for this. Everything is almost there!

char* getResourcePath(char* type);
void initAppCore();