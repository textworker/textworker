#include "tabber.h"
// #include "../appsettings.h"

#include <wx/stc/stc.h>
#include <wx/msgdlg.h>

Tabber::Tabber(wxWindow* parent)
{
    long style = wxAUI_NB_WINDOWLIST_BUTTON;
    
    // if (global_settings["editor"]["tabs"]["move_tabs"])
        style |= wxAUI_NB_TAB_MOVE;
    
    // if (global_settings["editor"]["tabs"]["close_on_middle_click"])
        style |= wxAUI_NB_MIDDLE_CLICK_CLOSE;
    
    // if (global_settings["editor"]["tabs"]["close_on_no_tab"])
        style |= wxAUI_NB_CLOSE_ON_ALL_TABS;
    // else
    //     style |= wxAUI_NB_CLOSE_ON_ACTIVE_TAB;

    Init();
    Create(parent, wxID_ANY, wxDefaultPosition, wxDefaultSize, style);

    AddTab(_("New Tab"));

    FileDialog = new wxFileDialog(this, '.', '/');

    Bind(wxEVT_AUINOTEBOOK_PAGE_CHANGED, &Tabber::OnPageChanged, this);
    Bind(wxEVT_AUINOTEBOOK_PAGE_CLOSED, &Tabber::OnPageClosed, this);
    Bind(wxEVT_CLOSE_WINDOW, &Tabber::OnDestroy, this);
}

void Tabber::AddTab(const wxString& tabname)
{
    StyledTextControl* newte = new StyledTextControl(this);
    newte->EditorInit();
    newte->SetZoom(3);

    // clrCall.Configure(static_cast<wxWindow*>(newte));
    newte->StyleClearAll();

    AddPage(newte, tabname, true);
    SetTitle(tabname);
}

void Tabber::SetTitle(wxString title)
{
    wxGetTopLevelParent(this)->SetLabel(title);
}

void Tabber::OnPageChanged(wxAuiNotebookEvent& evt)
{
    SetTitle(GetPageText(evt.GetSelection()));
}

void Tabber::OnPageClosed(wxAuiNotebookEvent& WXUNUSED(evt)) {}
void Tabber::OnDestroy(wxCloseEvent& evt) {}