#include <wx/aui/auibook.h>
#include <wx/filedlg.h>
#include <libtextworker/interface/wxGUI/editor.h>

using namespace libtextworker::wx;

class Tabber: public wxAuiNotebook
{
public:

    Tabber(wxWindow* parent);

    void AddTab(const wxString& tabname = "New tab");
    void SetTitle(wxString title);

    wxString AskToOpen()
    {
        FileDialog->SetMessage(_("Open a file"));
        if ( FileDialog->ShowModal() == wxID_OK )
        {
            wxString ret = FileDialog->GetPath();
            static_cast<StyledTextControl>(this->GetCurrentPage()).SaveFile(ret);
            return ret;
        }
        return wxEmptyString;
    };

    wxString AskToSave()
    {
        FileDialog->SetMessage(_("Save this as"));
        if ( FileDialog->ShowModal() == wxID_OK )
        {
            wxString ret = FileDialog->GetPath();
            static_cast<StyledTextControl>(this->GetCurrentPage()).SaveFile(ret);
            return ret;
        }
        return wxEmptyString;
    };

    void OpenFile(wxString path)
    {
        if (static_cast<StyledTextControl>(this->GetCurrentPage()).IsModified()) { this->AddTab(); }
        static_cast<StyledTextControl>(this->GetCurrentPage()).LoadFile(path);
        this->SetPageText(this->GetSelection(), path);
        SetTitle(path);
    }

    void SaveFile(wxString path)
    {
        static_cast<StyledTextControl>(this->GetCurrentPage()).SaveFile(path);
    }

private:
    wxFileDialog* FileDialog;

protected:
    void OnPageChanged(wxAuiNotebookEvent& evt);
    void OnPageClosed(wxAuiNotebookEvent& WXUNUSED(evt));
    void OnDestroy(wxCloseEvent& evt);
};