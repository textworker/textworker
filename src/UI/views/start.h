#include <wx/wizard.h>
#include <wxMaterialDesignArtProvider.hpp>
#include <wxFluentuiRegularArt.hpp>
#include "startpage.h"
#include "../../appsettings.h"

class StartWizardPage: public wxWizardPageSimple
{
public:
    StartWizardPage(wxWizard* parent);
};

class NewProjectWizardPage: public wxWizardPageSimple
{
public:
    NewProjectWizardPage(wxWizard* parent);
};

class NewFileWizardPage: public wxWizardPageSimple
{
// public:
//     NewFileWizardPage(wxWizard* parent);
};

class Wizard: public wxWizard
{
public:
    Wizard(wxWindow* fm);
    wxWizardPageSimple* GetFirstPage() { return m_page1; };

private:
    StartWizardPage* m_page1;
};