#include "start.h"

#ifndef WX_PRECOMP
#   include <wx/checkbox.h>
#   include <wx/stattext.h>
#   include <wx/button.h>
#   include <wx/panel.h>
#   include <wx/commandlinkbutton.h>
#   include <wx/sizer.h>
#   include <wx/listctrl.h>
#   include <wx/combobox.h>
#   include <wx/textctrl.h>
#endif

Wizard::Wizard(wxWindow* fm)
{
    Create(fm);
    SetSize(900, 700);
    
    m_page1 = new StartWizardPage(this);
    NewProjectWizardPage* m_page2 = new NewProjectWizardPage(this);

    m_page1->SetNext(m_page2);
    m_page2->SetPrev(m_page1);

    // FitToPage(m_page1);
}

StartWizardPage::StartWizardPage(wxWizard* parent): wxWizardPageSimple(parent)
{
    // main box
    wxBoxSizer* mainbox = new wxBoxSizer(wxVERTICAL);

    wxStartPage* startp = new wxStartPage(this, -1, recentsList);
    
    wxWindowID createnew = startp->AddButton(wxART_NEW, "Create new project");
    auto openexisting = startp->AddButton(wxART_FILE_OPEN, "Open a file/project");
    auto opendir = startp->AddButton(wxART_OPEN_FOLDER, "Open a folder");
    auto continueblank = startp->AddButton(wxART_FORWARD, "Continue without opening anything");
    
    startp->Bind(wxEVT_STARTPAGE_CLICKED, [=](wxCommandEvent& evt) {
        int id = evt.GetId();
        if (id == createnew) { parent->ShowPage(this->GetNext()); }
        if (id == continueblank) { parent->Close(); }
        evt.Skip();
    });

    mainbox->Add(startp, wxSizerFlags(1).Expand());

    this->SetSizer(mainbox);
    this->Layout();
    this->Centre();
}

NewProjectWizardPage::NewProjectWizardPage(wxWizard* parent)
                    : wxWizardPageSimple(parent)
{
    // main box
    wxBoxSizer* mainbox = new wxBoxSizer(wxVERTICAL);

    // content boxes
    wxBoxSizer* contentbox = new wxBoxSizer(wxHORIZONTAL);
    mainbox->Add(contentbox, 1, wxEXPAND, 5);

    wxPanel* leftpanel = new wxPanel(this);
    wxPanel* rightpanel = new wxPanel(this);
    contentbox->Add(leftpanel, 1, wxEXPAND, 5);
    contentbox->AddSpacer(1);
    contentbox->Add(rightpanel, 1, wxEXPAND, 5);

    wxBoxSizer* leftbox = new wxBoxSizer(wxVERTICAL);
    wxBoxSizer* rightbox = new wxBoxSizer(wxVERTICAL);
    leftpanel->SetSizer(leftbox);
    rightpanel->SetSizer(rightbox);

    /* LEFT PANE */
    leftbox->AddSpacer(1);
    wxStaticText* startlabel = new wxStaticText(leftpanel, -1, "Create a new project");
    startlabel->SetFont(wxFont(18, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, false, wxEmptyString));
    leftbox->Add(startlabel, 0, wxALL, 5);
    leftbox->AddSpacer(1);

    leftbox->Add(new wxStaticText(leftpanel, -1, "Name"), wxSizerFlags().Expand().Left());
    wxTextCtrl* namebox = new wxTextCtrl(leftpanel, -1);
    leftbox->Add(namebox, wxSizerFlags().Expand());

    leftbox->Add(new wxStaticText(leftpanel, -1, "Type"), wxSizerFlags().Expand().Left());
    wxComboBox* projtype = new wxComboBox(
        leftpanel, -1, wxEmptyString, wxDefaultPosition,
        wxDefaultSize, { _("Console"), _("GUI"), _("Library") },
        wxCB_READONLY);
    leftbox->Add(projtype, wxSizerFlags().Expand());

    leftbox->Add(new wxStaticText(leftpanel, -1, "Language"), wxSizerFlags().Expand().Left());
    wxComboBox* projlang = new wxComboBox(
        leftpanel, -1, wxEmptyString, wxDefaultPosition,
        wxDefaultSize, { _("Python"), _("C++"), _("C") },
        wxCB_READONLY);
    leftbox->Add(projlang, wxSizerFlags().Expand());

    /* RIGHT PANE */
    rightbox->AddSpacer(1);
    wxStaticText* rrrr = new wxStaticText(rightpanel, -1, "Options");
    rrrr->SetFont(wxFont(18, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, false, wxEmptyString));
    rightbox->Add(rrrr, 0, wxEXPAND, 5);
    rightbox->AddSpacer(1);

    wxCheckBox* enable_deb = new wxCheckBox(rightpanel, -1, "Enable .deb packaging");
    rightbox->Add(enable_deb, wxSizerFlags().Expand());

    wxCheckBox* enable_vcs = new wxCheckBox(rightpanel, -1, "Enable version control (Git by default)");
    rightbox->Add(enable_vcs, wxSizerFlags().Expand());

    rightbox->Add(new wxStaticText(rightpanel, -1, "Choose a build system.\nPython will use PyPa's build by default, options below won't replace it."), wxSizerFlags().Expand().Left());
    wxComboBox* buildsys = new wxComboBox(
        rightpanel, -1, wxEmptyString, wxDefaultPosition,
        wxDefaultSize, { _("Makefile"), _("CMake"), _("Meson") },
        wxCB_READONLY);
    rightbox->Add(buildsys, wxSizerFlags().Expand());

    this->Layout();
    this->Centre();
}