#include "mainwindow.h"
#include "start.h"
#include "../../cmdline.h"
#include "preferences.h"

#include <fstream>
#include <libtextworker/general.h>
#include <string>
#include <wx/aboutdlg.h>
#include <wx/utils.h>

using namespace libtextworker::general;

MainWindow::MainWindow()
            : mainFrame(NULL, -1, "TextWorker")
{
    Maximize();
    tabber = new Tabber(this);
    file_history = new wxFileHistory();
    auimgr = new wxAuiManager(this, wxAUI_MGR_ALLOW_FLOATING | wxAUI_MGR_ALLOW_ACTIVE_PANE);
    auimgr->AddPane(
        tabber, wxAuiPaneInfo().Caption("Notebook")
                               .MinimizeButton(true).MaximizeButton(true)
                               .PinButton(true).CenterPane());
    auimgr->Update();

    BindMenuEvents();
    file_history->UseMenu(recents);
    
    // Append items to the Recents menu.
    // As I have not made any new IDs for this (each menu item) will have an ID
    // (probably so), and 9 is, in fact, fairly enough, 9 will be used as maximum items count.
    
    file_history->AddFilesToMenu(recents);
    Show();
    Wizard st = new Wizard(this);
    st.ShowPage(st.GetFirstPage());
    st.ShowModal();
}

void MainWindow::BindMenuEvents()
{
    // Overloads of Bind() here are odd:
    // You can't use Bind(wxEVT_MENU, lambda, menu item, ID)

    // Contributors: The index number that the menu tracks must match the coresponding one
    // in mainwindow.fbp (aka UI design) or its generated C++ code (mainwindow_generated.cpp).
    // Separators still count, and of course they are separators. No Bind() for them.
    // If unsure, open the .fbp file, look at the object tree, do your work, and regenerate C++ files.

    /* FILE MENU */

    Bind(wxEVT_MENU, [this](wxCommandEvent&) { tabber->AddTab(); }, wxID_NEW);
    Bind(wxEVT_MENU, [this](wxCommandEvent&) { }, wxID_ADD, -1, filemenu->FindItemByPosition(1));
    Bind(wxEVT_MENU, [this](wxCommandEvent&) { }, ID_NEW_WIND);
    // separator
    Bind(wxEVT_MENU, [this](wxCommandEvent&) { file_history->AddFileToHistory(tabber->AskToOpen()); }, wxID_OPEN);
    // open folder
    // Bind(wxEVT_MENU, &MainWindow::OnFileHistory, wxID_FILE1, wxID_FILE9);
    // separator
    Bind(wxEVT_MENU,
         [this](wxCommandEvent&) {
                int currSel = tabber->GetSelection();
                static_cast<StyledTextControl>(tabber->GetPage(currSel))
                    .SaveFile(tabber->GetPageText(currSel));
         },
         wxID_SAVE);
    Bind(wxEVT_MENU, [this](wxCommandEvent&) { tabber->AskToSave(); }, wxID_SAVEAS);
    // separator
    Bind(wxEVT_MENU, [this](wxCommandEvent&) { tabber->DeleteAllPages(); }, wxID_CLOSE_ALL, -1, filemenu->FindItemByPosition(11));
    Bind(wxEVT_MENU, [this](wxCommandEvent&) { this->Close(); }, wxID_EXIT, -1, filemenu->FindItemByPosition(12));

    /* SETTINGS MENU */

    Bind(wxEVT_MENU, [this](wxCommandEvent&) { PreferencesDialog(this).ShowModal(); }, wxID_PREFERENCES, -1, settingsmenu->FindItemByPosition(0));

    /* HELP MENU */
    
    Bind(wxEVT_MENU, [this](wxCommandEvent&){ OnAbout(); }, wxID_ABOUT, -1, helpmenu->FindItemByPosition(0));
    Bind(wxEVT_MENU, [this](wxCommandEvent&){ OnOpenIssues(); }, ID_REPORT, -1, helpmenu->FindItemByPosition(4));
    Bind(wxEVT_MENU, [this](wxCommandEvent&){ OnOpenHomePage(); }, wxID_INDEX, -1, helpmenu->FindItemByPosition(5));
}

/*
 * Event callbacks
 */

void MainWindow::OnCloseWindow(wxCloseEvent& evt)
{
    auimgr->UnInit();
    if (file_history->GetCount() > 0)
    {
        std::ofstream history_file(appDir() + ".history", std::ios::trunc);
        for (size_t i = 0; i < file_history->GetCount(); i++)
        {
            history_file << file_history->GetHistoryFile(i);
            history_file << "\n";
        }
        history_file.close();
    }
    evt.Skip();
}


void MainWindow::OnFileHistory(wxCommandEvent& evt)
{
    wxString path = file_history->GetHistoryFile(evt.GetId() - wxID_FILE1);
    tabber->OpenFile(path);
    file_history->AddFileToHistory(path);
    evt.Skip();
}

void MainWindow::OnAbout()
{
    wxAboutDialogInfo aboutInfo;
    aboutInfo.SetVersion(CMDLINE_PARSER_VERSION);
    aboutInfo.SetDescription(_("A cross-platform editor"));
    aboutInfo.SetCopyright("(C) 2024");
    aboutInfo.SetWebSite("https://gitlab.com/textworker");
    aboutInfo.AddDeveloper("Le Bao Nguyen"); // TODO

    wxAboutBox(aboutInfo, this);
}