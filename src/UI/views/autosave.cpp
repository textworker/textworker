#include <thread>
#include <functional>
#include <algorithm>
#include <wx/wx.h>

#include "autosave.h"
#include "../../appsettings.h"

bool AS_Enabled() {
    std::any what_we_will_have = global_settings.Get("editor.autosave", "enable", true, true, true, true);
    return std::any_cast<bool>(what_we_will_have);
}

int AS_DelaySecs() {
    return std::any_cast<int>(global_settings.Get("editor.autosave", "time"));
}

AutoSaveDlg::AutoSaveDlg(wxWindow* parent)
            : AutoSaveDialog(parent)
{
    for (auto item : timealiases)
    { this->m_comboBox1->Append(item.first); }

    Bind(wxEVT_COMBOBOX, &AutoSaveDlg::OnChoiceSelected, this);
};

void AutoSaveDlg::OnChoiceSelected(wxCommandEvent& evt)
{
    std::string choice = m_comboBox1->GetValue();
    if (!choice.empty())
    {
        if (m_checkBox1->GetValue())
            global_settings.SetAndUpdate("editor.autosave", "time", timealiases[choice]);
        else
            global_settings.Set("editor.autosave", "time", timealiases[choice]);
    }
}