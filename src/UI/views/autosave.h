#include <wx/wxprec.h>
#include <map>

#ifndef WX_PRECOMP
#   include <wx/wx.h>
#endif

#include "autosave_generated.h"

bool AS_Enabled();
int AS_DelaySecs();

class AutoSaveDlg: public AutoSaveDialog
{
public:
    AutoSaveDlg(wxWindow* parent);

protected:
    std::map<std::string, int> timealiases = {
        { _("10 seconds"),   10 },
        { _("30 seconds"),   30 },
        { _("1 minute"),     60 },
        { _("2 minutes"),   120 },
        { _("5 minutes"),   300 },
        { _("10 minutes"),  600 },
        { _("15 minutes"),  900 },
        { _("20 minutes"), 1200 },
        { _("30 minutes"), 1800 },
    };

    void OnChoiceSelected(wxCommandEvent& evt);
};