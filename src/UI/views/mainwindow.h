#include "mainwindow_generated.h"
#include "../widgets/tabber.h"
#include "../../appsettings.h"

#include <wx/frame.h>
#include <wx/aui/aui.h>
#include <wx/filehistory.h>

class MainWindow: public mainFrame
{
public:
    wxAuiManager* auimgr;
    Tabber* tabber;
    wxFileHistory* file_history;

    MainWindow();

private:
    // bind menu events
    void BindMenuEvents();

    // event callbacks
    void OnAbout();
    void OnDirSelect(wxCommandEvent& evt) {};
    void OnFileSelect(wxCommandEvent& evt) {};
    void OnSysInfoShow(wxCommandEvent& evt) {};
    void OnFileHistory(wxCommandEvent& evt);
    void OnCloseWindow(wxCloseEvent& evt);
    void OnOpenIssues()
    {
        wxLaunchDefaultBrowser("https://gitlab.com/textworker/textworker/issues");
    }
    void OnOpenHomePage()
    {
        wxLaunchDefaultBrowser("https://gitlab.com/textworker/textworker/");
    }
};