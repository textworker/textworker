#include "appsettings.h"
#include <libtextworker/general.h>
#include <filesystem>

using namespace libtextworker::general;
using namespace libtextworker::get_config;
using namespace libtextworker::UI::wx;

std::string appDir() {
    return
#if defined(__APPLE__)
        std::string(getHomePath()) + "/Library/textworker/";
#else
    #if defined(__WIN32__)
        std::string(getHomePath()) + R"(\AppData\Local\textworker\)";
    #else
        std::string(getHomePath()) + "/.local/share/textworker/";
    #endif
#endif
}

char* getResourcePath(char* type)
{
    const Json::Value dictionary = global_settings["config-paths"];
    const std::string name = dictionary[(type == "editor") ? "name" : "theme"].asString();
    std::string path = dictionary["path"].asString();

    if (path == "unchanged")
    {
        path = CraftItems({libtextworker::TOPLV_DIR, (type == "ui") ? "themes" : type});
    }
    return CraftItems({path.c_str(), name.c_str()}).data();
}

void initAppCore() {
    libtextworker::TOPLV_DIR = appDir().c_str();
    // TODO: JSON Schema
    // TODO: Profiles (don't know if you will use this)
    global_settings.Create({}, CraftItems({libtextworker::TOPLV_DIR, "configs.json"}), true);
    clrCall.Create({}, getResourcePath("ui"), true);
    editorCfg.Create({}, getResourcePath("editorconfigs"), true);
}