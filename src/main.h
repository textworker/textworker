#pragma once

#include <vector>
#include <wx/wxprec.h>

#ifndef WX_PRECOMP
#   include <wx/wx.h>
#endif

std::vector<const char*> input_files;
std::vector<const char*> input_dirs;
std::vector<const char*> input_non_existants;
bool autocreate;
bool ignore_non_existants;

// wxApp derive

class TEApp: public wxApp
{
private:
    bool showSplash = true;

public:
    virtual bool OnInit() override;
};