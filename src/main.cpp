#include <fstream>
#include <libintl.h>
#include <stdlib.h>
#include <string>
#include <stdio.h>

#include <wx/aui/aui.h>
#include <wx/splash.h>
#include <wxMaterialDesignArtProvider.hpp>
#include <libtextworker/general.h>

#include "main.h"
#include "cmdline.h"
#include "appsettings.h"
#include "UI/views/mainwindow.h"

#include "splash.xpm"

using namespace libtextworker::general;

bool TEApp::OnInit()
{
    wxInitAllImageHandlers();
    wxArtProvider::Push(new wxMaterialDesignArtProvider);
    SetAppDisplayName("TextWorker");
    
    WalkCreation(appDir().c_str());

    initAppCore();

    std::ifstream readf(appDir() + ".history");
    std::string line;
    while (std::getline(readf, line))
    {
        // TODO: Resolve relative path
        recentsList.Add(line);
    }
    readf.close();


    if (showSplash) {
        new wxSplashScreen(
            wxBitmap(splash), wxSPLASH_CENTRE_ON_SCREEN | wxSPLASH_TIMEOUT, 5000,
            NULL, wxID_ANY, wxDefaultPosition, wxDefaultSize,
            wxSIMPLE_BORDER | wxSTAY_ON_TOP);
    }

    MainWindow* fm = new MainWindow();

    SetTopWindow(fm);
    return true;
}

wxIMPLEMENT_APP_NO_MAIN(TEApp);
wxIMPLEMENT_WX_THEME_SUPPORT;

int main(int argc, char** argv)
{
    /* Parse command-line arguments */
    gengetopt_args_info args_info;

    // never got this thing
    if (cmdline_parser(argc, argv, &args_info) != 0)
        exit(1);
    
    // file-related flags
    if (args_info.auto_create_given) {
        autocreate = args_info.auto_create_flag;
    }
    
    if (args_info.ignore_non_existants_given) {
        ignore_non_existants = args_info.ignore_non_existants_flag;
    }
    
    if (autocreate && ignore_non_existants)
    {
        fprintf(stderr, "--auto-create and --ignore-non-existants are used together");
        return 1;
    }

    // passed paths
    for (unsigned i = 0; i < args_info.inputs_num; i++)
    {
        switch (exists(args_info.inputs[i]))
        {
        case 0:
            input_files.push_back(args_info.inputs[i]);
            break;
        case 1:
            input_dirs.push_back(args_info.inputs[i]);
            break;
        case -1:
            input_non_existants.push_back(args_info.inputs[i]);
            break;
        }
    }

    /* After parsing arguments, we can initialize wxApp just fine. */
    wxEntryStart(argc, argv);
    wxTheApp->CallOnInit();
    wxTheApp->OnRun();

    return 0;
}
