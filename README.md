## TextWorker

An universal IDE.

## Dependencies

* `libtextworker` 1.0

* `wxWidgets` >= 3.2.0 (I recommend 3.2.5 for macOS 14, read its change log)

* `meson` and `ninja`

* `windres` on Windows

* `ImageMagick` (optional, for splash screens)

* `GenGetopt` (for generating command-line files)

* `wxFormBuilder` (for generating files)

## Build

1. Generate new build directory: `meson build`

> `build` is the target build directory, change it also for commands below (after `ninja -C`).
> You can just `cd build` and remove `-C build` from ninja.

2. Update translations strings (optional, requires `gettext`): `ninja -C build textworker-pot` then `ninja -C build textworker-update-po`

3. Build: `ninja -C build`.

4. Install: `ninja -C build install`.